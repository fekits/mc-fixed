// import { call, put, all, select } from 'redux-saga/effects';

// 默认数据
export default {
  // 是否准备就绪
  ready: null,
  // 是否已经登录
  logged: 1,
  // 登录帐号信息
  account: null,
  // 语言列表
  langs: {},
  // 词库
  words: {}
};
