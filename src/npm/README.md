## @fekit/fixed

指定元素滚动到窗口指定位置时固定在指定位置。

#### 索引

- [演示](#演示)
- [参数](#参数)
- [示例](#示例)
- [版本](#版本)
- [反馈](#反馈)

#### 演示

[https://plugins.fekit.cn/@fekit/fixed/](https://plugins.fekit.cn/@fekit/fixed/)

#### 开始

下载项目:

```
npm i @fekit/fixed
```

#### 参数

```$xslt
  @param  param              {Object}    * 入参
  @param  param.el           {Element}   * 需要监听的DOM节点
  @param  param.area         {Element}     作用范围区域 (选填，默认为body)
  @param  param.wrap         {Element}     动态参考元素 (选填，如不填写则为其父元素)
  @param  param.top          {String}      偏移量值 (选填，默认为0)
  @param  param.fixedStyle   {String}      固定时的样式
  @param  param.fixedClass   {String}      固定时的CLASS
  @param  param.autoStyle    {Boolean}     自是开启动动样式
  @param  param.on           {Object}      回调集
  @param  param.fixed        {Function}    固定时
  @param  param.reduced      {Function}    还原时
```

#### 示例

前置条件

```html
// 使用此插件，必须在需要固定的元素外面包一层容器，且此容器尺寸与位置必须和固定元素一样大小或不设置任何尺寸与位置属性。
<div>
  <div id="el"></div>
</div>
```

```javascript
// 楼层埋点插件
import McFixed from '@fekit/fixed';

// 新建一个在ID为f3的区域范围内滚动到顶部60px时固定的实例。
new McFixed({
  el: document.getElementById('f3_tab'),
  area: document.getElementById('f3'),
  zIndex: 999,
  top: 60,
});

// 设置一个不设定作用范围的实例,默认为整个body区域，顶部为0时固定的实例，并且设置层级为1001,固定时触发一个回调。
new McFixed({
  el: document.getElementById('f1_tab'),
  fixedClass: 'f1_fixed', // 在这个设置的class下写固定后的样式
  on: {
    fixed() {
      console.log(_this);
    },
  },
});
```

#### 版本

```$xslt
v2.0.0 [Latest version]
1. TS重构
```

```$xslt
v1.1.2
1. 优化插件对外部使用环境的容错能力
```

```$xslt
v1.1.0
1. 优化当导航尺寸发生变化时的自适应功能
```

```$xslt
v1.0.8
1. 优化当窗口发生变化时的自适应功能
```

```$xslt
v1.0.7
1. 优化当窗口发生变化时的自适应功能
```

```$xslt
v1.0.6
1. 还是那个className的bug
```

```$xslt
v1.0.5
1. 修复一个处理className的bug
```

```$xslt
v1.0.4
1. 原fixedCss入参属性名改为fixedStyle。用法未变。
2. 新增一个fixedClass入参，当固定时设置一个class，这个class可以预先写好固定时的样式。当设有fixedClass时fixedStyle仅设置top，其它样式需要在class中写样式。
3. 固定或还原状态切换时仅触发一次事件。原来是只要滚动就不停的触发固定或还原事件。
```

```$xslt
v1.0.3
1. 非核心优化
```

```$xslt
v1.0.2
1. 非核心优化
```

```$xslt
v1.0.1
1. 修复安卓兼容性问题
2. 优化指定区域内显示的元素在区域滚动到底部时元素跟随区域底部渐渐滚出可视区外。
```

```$xslt
v1.0.0
1. 此版本完成了核心功能。
```

#### 反馈

```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
